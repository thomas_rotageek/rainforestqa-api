﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class TestHistory
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public int run_id { get; set; }
        public List<Browser> browsers { get; set; }
        public int environment_id { get; set; }
    }

    public class Browser
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public string name { get; set; }
        public string result { get; set; }
        public string state { get; set; }
    }

    
}
