﻿using ConsoleApplication1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Web;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json.Linq;

namespace APIRainforestQA
{
    class Program
    {

        static void Main(string[] args)
        {
            List<Test> tests = GetTestsByTag("Positions");

            SetTestHistory(tests);

            FilterTests("", "", "", tests);
        }

        public static List<Test> FilterTests(string browser, string environment, string result, List<Test> tests)
        {
            //TODO: make this one LINQ statement

            /*
             * 
             * 
              For each test in tests
                filter the test history by environment
                Then for each browser in test history filter for browser and result
                if there are no matches then test.testHistory.browsers will be null    

              tests = tests.Where(test.testHistory.browsers is not null)
              
             */


            return tests; //delete later


        }


        public static void SetTestHistory(List<Test> tests)
        {
            foreach (Test test in tests)
            {
                test.testHistory = GetTestHistoryById(test.test_id);
            }
        }

        public static List<TestHistory> GetTestHistoryById(int test_id)
        {
            string url = "https://app.rainforestqa.com:443/api/1/tests/" + test_id.ToString() + "/history.json?page_size=100";
            string results = GetResultsFromRainforest(url);

            var testHistory = JsonConvert.DeserializeObject<List<TestHistory>>(results);

            return testHistory;
        }



        public static List<Test> GetTestsByTag(string tag)
        {
            string url = "https://app.rainforestqa.com:443/api/1/tests.json?page_size=100&tags=" + tag;
            string results = GetResultsFromRainforest(url);

            var tests = JsonConvert.DeserializeObject<List<Test>>(results);

            return tests;
        }

        public static string GetResultsFromRainforest(string url)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(url);
            webrequest.Method = "GET";
            webrequest.ContentType = "application/json";
            webrequest.Headers.Add("CLIENT_TOKEN", "8b653489a3f733491312c3c42022736f");
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);


            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();
            return result;
        }


    }
}
