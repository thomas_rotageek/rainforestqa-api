﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Test
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public int test_id { get; set; }
        public string rfml_id { get; set; }
        public int site_id { get; set; }
        public string title { get; set; }
        public string result { get; set; }
        public List<TestHistory> testHistory { get; set; }
    }
}
